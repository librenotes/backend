## Development
You will need to run the postgres daemon to use django in development, but if you want to use the 
postgres docker container, start it with `make postgres-start` and change `POSTGRES_HOST` to 127.0.0.1 in postgres envfile.
Then you will be able to connect development django server to the  posgres container by typing `make postgre-ip` and copying the resulting ip address to your development django env variables. 

In development mode, you **will not** be able to run nginx (NAT and firewall is a dealbreaker for letsencrypt)

To test your apps, you will have to use the default django development server (python manage.py runserver).

To setup python dev env use [poetry](https://python-poetry.org).
* `poetry install`
* `poetry shell`

## Testing
* Start the local server
* `cd test` and run `py.test`

## Configuration
Configuration files are inside .envs folder. By default, dev.env files are loaded for each container.
Config options are self explanatory, but there are some important things to mention:
* DB config in django envs folder must be the same as it is in the postgres config.
* Do not choose the username 'postgres' for the postgres config.
* DJANGO_ALLOWED_HOSTS should contain '.example.com' to accept all hosts from nginx.
* **important:** NGINX_HOST must just include your base domain. If you have `www.example.com`
  it must be set as `example.com` . This is because when certbot generates the certificates, it will
  use your base domain to generate a certificate valid for your www domain and non www domain.
* If you want to create more subdomains, you will have to change the file in config>nginx>default.template
  and pass the correct env variables plus adding the correct directives. Also, add your subdomain to the certbot command.
* IS_STAGING must be set to 1 if you want to test your project, to avoid rate limiting from letsencrypt.
  In production, change it to 0

## Production
Once you have your project done, it's time to go into production mode!
* Export poetry lock file to pip deps file `poetry export -f requirements.txt > requirements.txt`
* Inside your project folder, build your containers by typing `make build`
* Once it is finished, put it online with `make rund`


It is a good practice, on the first run, to check for any errors.. To do so, start your project with `make run`
If you see no errors, it is probably safe to start it normally with `make rund`

Use `make help` to show all available commands.