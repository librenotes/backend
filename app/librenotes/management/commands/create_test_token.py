from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

from rest_framework.authtoken.models import Token

UserModel = get_user_model()


class Command(BaseCommand):
    help = "Create null DRF Token for a given user for testing"

    def create_user_token(self, username):
        user = UserModel._default_manager.get_by_natural_key(username)

        token = Token.objects.get_or_create(user=user)
        token[0].delete()
        token = Token.objects.create(user=user, key="0000000000000000000000000000000000000000")

        return token

    def add_arguments(self, parser):
        parser.add_argument("username", type=str)

    def handle(self, *args, **options):
        username = options["username"]

        try:
            token = self.create_user_token(username)
        except UserModel.DoesNotExist:
            raise CommandError("Cannot create the Token: user {} does not exist".format(username))
        self.stdout.write("Generated token {} for user {}".format(token.key, username))
