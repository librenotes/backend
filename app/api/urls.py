from django.urls import include, path
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.routers import DefaultRouter

from . import views
from . import api_views


class OptionalSlashRouter(DefaultRouter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.trailing_slash = "/?"


router = OptionalSlashRouter()
router.register("notes", views.NoteViewSet)
router.register("tags", views.TagViewSet)
router.register("sync", views.SyncViewSet, basename="Sync")

urlpatterns = [
    path("", include(router.urls)),
    path("auth/", include("rest_framework.urls")),
    path("register/", api_views.UserCreateAPIView.as_view()),
    path("token/", ObtainAuthToken.as_view()),
]
